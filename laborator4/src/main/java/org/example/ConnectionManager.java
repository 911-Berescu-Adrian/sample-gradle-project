package org.example;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;

public class ConnectionManager extends Thread{
    private ServerSocket serverSocket;
    private Set<MessageSender> messageSenders = new HashSet<>();

    public ConnectionManager(String portString) throws IOException {
        serverSocket = new ServerSocket(Integer.parseInt(portString));
    }

    public void run(){
        try {
            while (true) {
                MessageSender messageSender = new MessageSender(serverSocket.accept(), this);
                messageSenders.add(messageSender);
                messageSender.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void sendMessage(String message) {
        try {
            messageSenders.forEach(t -> t.getPrintWriter().println(message));
        }
        catch (Exception e) {
            System.out.println("Error sending message: " + e.getMessage());
        }
    }

    public Set<MessageSender> getMessageSenders(){
        return messageSenders;
    }

    public void removeConnection(int port) {
        messageSenders.removeIf(thread -> {
            try {
                if (thread.getSocket().getPort() == port) {
                    thread.closeSocket();
                    return true;
                }
                return false;
            } catch (SocketException e) {
                System.out.println("Error while trying to disconnect");
                return false;
            }
        });
    }
}
