package org.example;

import javax.json.Json;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;
import java.net.SocketException;

public class Peer {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter your username:");
        String username = bufferedReader.readLine();
        System.out.println("Enter your port:");
        String port = bufferedReader.readLine();
        ConnectionManager connectionManager = new ConnectionManager(port);
        connectionManager.start();
        new Peer().updateListenToPeers(bufferedReader, username, connectionManager);
    }

    public void updateListenToPeers(BufferedReader bufferedReader, String username, ConnectionManager connectionManager) throws IOException {
        System.out.println("Connect to a peer by typing !hello <port_number> or !s to skip");
        String[] input = bufferedReader.readLine().split(" ");
        if (!input[0].equals("!s")) {
            for (String argument : input) {
                if (argument.equals("!hello")) continue;
                Socket socket = null;
                try {
                    socket = new Socket("localhost", Integer.parseInt(argument));
                    MessageReader messageReader = new MessageReader(socket);
                    messageReader.start();
                } catch (IOException e) {
                    if (socket != null) {
                        socket.close();
                    } else {
                        System.out.println("Couldn't find a valid port.");
                    }
                }
            }
        }
        communicate(bufferedReader, username, connectionManager);
    }

    public void communicate(BufferedReader bufferedReader, String username, ConnectionManager connectionManager) {
        try {
            System.out.println("Type your message or !connect to connect to another peer or !bye <port_number> to disconnect from a peer or !byebye to exit");
            boolean flag = true;
            while(flag) {
                String message = bufferedReader.readLine();
                if("!byebye".equals(message)) {
                    flag = false;
                    break;
                } else if("!connect".equals(message)) {
                    updateListenToPeers(bufferedReader, username, connectionManager);
                } else if(message.startsWith("!bye")) {
                    int port = Integer.parseInt(message.split(" ")[1]);
                    connectionManager.removeConnection(port);
                    System.out.println("Closed connection with port: " + port);
                } else {
                    StringWriter stringWriter = new StringWriter();
                    Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder()
                            .add("username", username)
                            .add("message", message)
                            .build());
                    connectionManager.sendMessage(stringWriter.toString());
                }
            }
            System.exit(0);
        } catch (IOException e) {
            System.out.println("Error during communication");
        }
    }
}