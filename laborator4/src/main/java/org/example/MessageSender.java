package org.example;

import javax.json.Json;
import java.io.*;
import java.net.Socket;

public class MessageSender extends Thread {
    private volatile boolean running = true;
    private ConnectionManager connectionManager;
    private Socket socket;
    private PrintWriter printWriter;

    public MessageSender(Socket socket, ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        this.socket = socket;
        try {
            this.printWriter = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Error: Initializing  Connection");
        }
    }



    public void closeSocket() throws SocketException {
        try {
            running = false;
            if (socket != null && !socket.isClosed()) {
                socket.close();
                printWriter.close();
                throw new SocketException("Socket Exception: Peer disconnected.");
            }
        } catch (SocketException e) {
            System.out.println("Error while closing socket: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.printWriter = new PrintWriter(socket.getOutputStream(), true);
            StringWriter stringWriter = new StringWriter();
            Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder()
                    .add("username", "System")
                    .add("message", "!ack")
                    .build());
            printWriter.println(stringWriter.toString());
            System.out.println("Sent !ack to a connected peer");

            while (running) {
                connectionManager.sendMessage(bufferedReader.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
            connectionManager.getMessageSenders().remove(this);
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }
}
