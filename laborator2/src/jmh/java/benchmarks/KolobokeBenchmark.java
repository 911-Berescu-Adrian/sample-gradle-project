package benchmarks;

import org.example.Order;
import org.example.Repositories.KolobokeBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class KolobokeBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkHelper {
        KolobokeBasedRepository<Order> repository = new KolobokeBasedRepository<>();
        Order order = new Order(1, 50, 1);
        @Param({"1", "100"})
        public int size;

        @Setup(Level.Iteration)
        public void setup() {
            repository.clear();
            for (int i = 0; i < size; i++) {
                repository.add(order);
            }
        }
    }

    @Benchmark
    public void add(BenchmarkHelper bh) {
        for (int i = 0; i < bh.size; i++) {
            bh.repository.add(bh.order);
        }
    }

    @Benchmark
    public void remove(BenchmarkHelper bh) {
        for (int i = 0; i < bh.size; i++) {
            bh.repository.remove(bh.order);
        }
    }

    @Benchmark
    public void contains(BenchmarkHelper bh) {
        for (int i = 0; i < bh.size; i++) {
            bh.repository.contains(bh.order);
        }
    }
}

