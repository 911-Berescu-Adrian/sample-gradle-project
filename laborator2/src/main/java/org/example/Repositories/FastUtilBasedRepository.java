package org.example.Repositories;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class FastUtilBasedRepository<T> implements InMemoryRepository<T>{

    private final ObjectArrayList<T> list;

    public FastUtilBasedRepository() {
        list = new ObjectArrayList<>();
    }

    @Override
    public void add(T elem) {
        list.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return list.contains(elem);
    }

    @Override
    public void remove(T elem) {
        list.remove(elem);
    }

    @Override
    public void clear() {
        list.clear();
    }
}
