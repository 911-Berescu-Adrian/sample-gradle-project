package org.example.Repositories;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T>{

    private final Map<Integer, T> map;

    public ConcurrentHashMapBasedRepository() {
        map = new ConcurrentHashMap<>();
    }
    @Override
    public void add(T elem) {
        map.put(elem.hashCode(), elem);
    }

    @Override
    public boolean contains(T elem) {
        return map.containsKey(elem.hashCode());
    }

    @Override
    public void remove(T elem) {
        map.remove(elem.hashCode());
    }

    @Override
    public void clear() {
        map.clear();
    }
}
