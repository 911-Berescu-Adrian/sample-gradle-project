package org.example.Repositories;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

public class EclipseBasedRepository<T> implements InMemoryRepository<T> {
    private final MutableList<T> list;

    public EclipseBasedRepository() {
        this.list = FastList.newList();
    }

    @Override
    public void add(T elem) {
        list.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return list.contains(elem);
    }

    @Override
    public void remove(T elem) {
        list.remove(elem);
    }

    @Override
    public void clear() {
        list.clear();
    }
}
