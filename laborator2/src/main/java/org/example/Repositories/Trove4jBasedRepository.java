package org.example.Repositories;

import gnu.trove.set.hash.THashSet;

public class Trove4jBasedRepository<T> implements InMemoryRepository<T> {
    private final THashSet<T> set = new THashSet<>();

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }

    @Override
    public void clear() {
        set.clear();
    }

}
