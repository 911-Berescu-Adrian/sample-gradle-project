package org.example;

import java.util.List;
import java.util.stream.Collectors;

public class DoubleObjectOperations  implements ObjectOperations<Double>{

    @Override
    public Double sum(List<Double> numbers) {
        return numbers.stream().mapToDouble(Double::doubleValue).sum();
    }

    @Override
    public Double average(List<Double> numbers) {
        return numbers.stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
    }

    @Override
    public List<Double> top10Percent(List<Double> numbers) {
        int size = (int) (numbers.size() * 0.1);
        return numbers.stream()
                .sorted((a, b) -> Double.compare(b, a))
                .limit(size)
                .collect(Collectors.toList());
    }
}
