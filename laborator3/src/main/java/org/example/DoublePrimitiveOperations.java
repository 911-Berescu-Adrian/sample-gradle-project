package org.example;

import java.util.Arrays;

public class DoublePrimitiveOperations {

    public double sum(double[] numbers) {
        return Arrays.stream(numbers).sum();
    }

    public double average(double[] numbers) {
        return Arrays.stream(numbers).average().orElse(0.0);
    }

    public double[] top10Percent(double[] numbers) {
        int size = (int) (numbers.length * 0.1);
        return Arrays.stream(numbers)
                .sorted()
                .skip(numbers.length - size)
                .toArray();
    }
}
