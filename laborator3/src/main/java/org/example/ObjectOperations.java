package org.example;

import java.util.List;

public interface ObjectOperations <T extends Number>{
    T sum(List<T> numbers);

    T average(List<T> numbers);

    List<T> top10Percent(List<T> numbers);
}
