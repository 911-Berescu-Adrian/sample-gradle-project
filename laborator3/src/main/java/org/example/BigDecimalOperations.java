package org.example;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalOperations implements ObjectOperations<BigDecimal>{

    public BigDecimal sum(List<BigDecimal> numbers) {
        return numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average(List<BigDecimal> numbers) {
        return numbers.stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add).divide(new BigDecimal(numbers.size()), 2, BigDecimal.ROUND_HALF_UP);
    }

    public List<BigDecimal> top10Percent(List<BigDecimal> numbers) {
        int size = (int) (numbers.size() * 0.1);
        return numbers.stream()
                .sorted((a, b) -> b.compareTo(a))
                .limit(size)
                .collect(Collectors.toList());
    }
}

