package benchmarks;

import org.example.DoublePrimitiveOperations;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class DoublePrimitiveRandomBenchmark {
    @State(Scope.Benchmark)
    public static class BenchmarkHelper {
        double[] doublePrimitivelList;

        DoublePrimitiveOperations doublePrimitiveOperations = new DoublePrimitiveOperations();

        Random random = new Random();


        public int size = 1000;

        @Setup(Level.Iteration)
        public void setup() {
            doublePrimitivelList = new double[size];
            for (int i = 0; i < size; i++) {
                doublePrimitivelList[i] = random.nextDouble();
            }
        }
    }

    @Benchmark
    public void sumDoublePrimitive(BenchmarkHelper bh) {
        bh.doublePrimitiveOperations.sum(bh.doublePrimitivelList);
    }

    @Benchmark
    public void averageDoublePrimitive(BenchmarkHelper bh) {
        bh.doublePrimitiveOperations.average(bh.doublePrimitivelList);
    }

    @Benchmark
    public void topTenPercentDoublePrimitive(BenchmarkHelper bh) {
        bh.doublePrimitiveOperations.top10Percent(bh.doublePrimitivelList);
    }
}
