package benchmarks;

import org.openjdk.jmh.annotations.*;
import org.example.BigDecimalOperations;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class BigDecimalAscBenchmark {
    @State(Scope.Benchmark)
    public static class BenchmarkHelper {
        private List<BigDecimal> bigDecimalList = new ArrayList<>();
        BigDecimalOperations bigDecimalOperations = new BigDecimalOperations();


        public int size = 1000;

        @Setup(Level.Iteration)
        public void setup() {
            bigDecimalList.clear();
            for (int i = 0; i < size; i++) {
                bigDecimalList.add(BigDecimal.valueOf(i));
            }
        }
    }

    @Benchmark
    public void sumBigDecimal(BenchmarkHelper bh) {
        bh.bigDecimalOperations.sum(bh.bigDecimalList);
    }

    @Benchmark
    public void averageBigDecimal(BenchmarkHelper bh) {
        bh.bigDecimalOperations.average(bh.bigDecimalList);
    }

    @Benchmark
    public void topTenPercentBigDecimal(BenchmarkHelper bh) {
        bh.bigDecimalOperations.top10Percent(bh.bigDecimalList);
    }
}
