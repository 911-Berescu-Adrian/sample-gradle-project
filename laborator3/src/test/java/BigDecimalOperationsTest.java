import org.example.BigDecimalOperations;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BigDecimalOperationsTest {
    @Test
    void testSum() {
        List<BigDecimal> numbers = Arrays.asList(new BigDecimal("1.7"), new BigDecimal("2.5"), new BigDecimal("7.3"));
        assertEquals(new BigDecimal("11.5"), new BigDecimalOperations().sum(numbers));
    }

    @Test
    void testAverage() {
        List<BigDecimal> numbers = Arrays.asList(new BigDecimal("1.7"), new BigDecimal("2.5"), new BigDecimal("7.3"));
        assertEquals(new BigDecimal("3.83"), new BigDecimalOperations().average(numbers));
    }

    @Test
    void testTop10Percent() {
        List<BigDecimal> numbers = Arrays.asList(
                new BigDecimal("1.7"),
                new BigDecimal("2.5"),
                new BigDecimal("7.3"),
                new BigDecimal("5.39"),
                new BigDecimal("12.0"),
                new BigDecimal("8.9"),
                new BigDecimal("3.14"),
                new BigDecimal("6.75"),
                new BigDecimal("9.8"),
                new BigDecimal("4.5"),
                new BigDecimal("11.2"),
                new BigDecimal("10.6")
        );

        List<BigDecimal> top10Percent = new BigDecimalOperations().top10Percent(numbers);
        System.out.println(top10Percent);
        assertEquals(top10Percent, List.of(new BigDecimal("12.0")));
    }
}