import org.example.DoublePrimitiveOperations;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DoublePrimitiveOperationsTest {
    @Test
    void testSum() {
        double[] numbers = {1.7, 2.5, 7.3};
        assertEquals(11.5, new DoublePrimitiveOperations().sum(numbers));
    }

    @Test
    void testAverage() {
        double[] numbers = {1.7, 2.5, 7.3};
        assertEquals(3.83, new DoublePrimitiveOperations().average(numbers), 0.01);
    }

    @Test
    void testTop10Percent() {
        double[] numbers = {1.7, 2.5, 7.3, 5.39, 12.0, 8.9, 3.14, 6.75, 9.8, 4.5, 11.2, 10.6};
        double[] top10Percent = new DoublePrimitiveOperations().top10Percent(numbers);
        assertEquals(top10Percent[0], 12.0);
    }
}