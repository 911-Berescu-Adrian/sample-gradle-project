package com.example;

import com.example.Operations.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    public Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void addShouldComputeTheCorrectResult() throws CalculatorException {
        assertEquals(4, calculator.compute(new AddOperation(), 2, 2));
        assertEquals(-0.54, calculator.compute(new AddOperation(), -3.54, 3));
    }

    @Test
    void subtractShouldComputeTheCorrectResult() throws CalculatorException {
        assertEquals(0, calculator.compute(new SubtractOperation(),3, 3));
        assertEquals(-3.4, calculator.compute(new SubtractOperation(),0, 3.4));
    }

    @Test
    void multiplyShouldComputeTheCorrectResult() throws CalculatorException {
        assertEquals(26, calculator.compute(new MultiplyOperation(),13,2));
        assertEquals(0, calculator.compute(new MultiplyOperation(),13.3,0));
        assertEquals(27, calculator.compute(new MultiplyOperation(),13.5,2));
    }

    @Test
    void divideShouldComputeTheCorrectResult() throws CalculatorException {
        assertEquals(1, calculator.compute(new DivideOperation(),14.362, 14.362));
        assertEquals(13.45, calculator.compute(new DivideOperation(),26.9, 2));
    }

    @Test
    void divisionByZeroNotAllowed() {
        assertThrows(CalculatorException.class, () -> calculator.compute(new DivideOperation(),5, 0));
        assertThrows(CalculatorException.class, () -> calculator.compute(new DivideOperation(),12.34, 0));
    }

    @Test
    void minShouldComputeTheCorrectResult() throws CalculatorException {
        assertEquals(-5, calculator.compute(new MinOperation(),-5,5));
    }

    @Test
    void maxShouldComputeTheCorrectResult() throws CalculatorException {
        assertEquals(5, calculator.compute(new MaxOperation(),-5,5));
    }

    @Test
    void sqrtShouldComputeTheCorrectResult() throws CalculatorException {
        assertEquals(0, calculator.compute(new SqrtOperation(),0, 0));
        assertEquals(9, calculator.compute(new SqrtOperation(),81, 0));
    }

    @Test
    void sqrtOfNegativesNotAllowed() {
        assertThrows(Exception.class, () -> calculator.compute(new SqrtOperation(),-45, 0));
    }
}