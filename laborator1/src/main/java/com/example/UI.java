package com.example;

import com.example.Operations.*;

import java.util.Scanner;

public class UI {
    private static final Scanner scanner = new Scanner(System.in);
    private final Calculator calculator;

    public UI(Calculator calculator) {
        this.calculator = calculator;
    }

    public void menu() {
        System.out.println("Select an operation:");
        System.out.println("1. add");
        System.out.println("2. subtract");
        System.out.println("3. multiply");
        System.out.println("4. divide");
        System.out.println("5. min");
        System.out.println("6. max");
        System.out.println("7. square root");
        System.out.println("0. exit");
    }

    public double readNumber(String number) {
        System.out.printf("Enter the %s number: ", number);
        return scanner.nextDouble();
    }

    public void start() {
        while (true) {
            menu();
            int command = scanner.nextInt();
            if (command == 0)
                break;
            double first = readNumber("first");
            double second = 0;
            if (command != 7) {
                second = readNumber("second");
            }
            double result = 0;
            try {
                switch (command) {
                    case 1 : result = calculator.compute(new AddOperation(), first, second); break;
                    case 2 : result = calculator.compute(new SubtractOperation(), first, second); break;
                    case 3 : result = calculator.compute(new MultiplyOperation(), first, second); break;
                    case 4 : result = calculator.compute(new DivideOperation(), first, second); break;
                    case 5 : result = calculator.compute(new MinOperation(), first, second); break;
                    case 6 : result = calculator.compute(new MaxOperation(), first, second); break;
                    case 7 : result = calculator.compute(new SqrtOperation(), first, 0); break;
                    default:
                        System.out.println("Please only select numbers from 0 to 7");
                }
                System.out.printf("Result: %.3f\n", result);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }


}
