package com.example;

import com.example.Operations.Operation;

public class Calculator {
    public double compute(Operation operation, double first, double second) throws CalculatorException {
        return operation.calculate(first, second);
    }
}
