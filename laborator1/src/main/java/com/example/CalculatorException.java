package com.example;

public class CalculatorException extends Exception{
    public CalculatorException(String message) {
        super(message);
    }
}
