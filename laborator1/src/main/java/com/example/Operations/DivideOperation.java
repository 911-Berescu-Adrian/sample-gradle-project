package com.example.Operations;

import com.example.CalculatorException;

public class DivideOperation implements Operation {
    @Override
    public double calculate(double first, double second) throws CalculatorException {
        if (second == 0) {
            throw new CalculatorException("Can't divide by 0.");
        }
        return first / second;
    }
}



