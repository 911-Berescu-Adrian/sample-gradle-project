package com.example.Operations;

public class MaxOperation implements Operation {
    @Override
    public double calculate(double first, double second) {
        return Math.max(first, second);
    }
}
