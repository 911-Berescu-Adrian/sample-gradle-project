package com.example.Operations;

import com.example.CalculatorException;

public interface Operation {
    double calculate (double first, double second) throws CalculatorException;
}

