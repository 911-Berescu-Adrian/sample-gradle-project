package com.example.Operations;

public class MinOperation implements Operation {
    @Override
    public double calculate(double first, double second) {
        return Math.min(first, second);
    }
}
