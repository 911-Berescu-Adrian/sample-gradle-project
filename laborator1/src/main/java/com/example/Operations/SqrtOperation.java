package com.example.Operations;

import com.example.CalculatorException;

public class SqrtOperation implements Operation {
    @Override
    public double calculate(double number, double second) throws CalculatorException {
        if (number < 0) {
            throw new CalculatorException("Can't get the sqrt of a negative number.");
        }
        return Math.sqrt(number);
    }
}
